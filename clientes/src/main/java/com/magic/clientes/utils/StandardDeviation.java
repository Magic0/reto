package com.magic.clientes.utils;


import java.util.Arrays;
import java.util.List;

public class StandardDeviation {
    public static Double computeStandardDeviation(List<Integer> collection) {
        if (collection.size() == 0) {
            return Double.NaN;
        }

    final double average =
            collection.stream()
        .mapToDouble(x -> x.doubleValue())
        .summaryStatistics()
        .getAverage();

    final double rawSum =
            collection.stream()
        .mapToDouble((x) -> Math.pow(x.doubleValue() - average,
        2.0))
        .sum();

        return Math.sqrt(rawSum / (collection.size() - 1));
    }
}