package com.magic.clientes.service.impl;

import com.magic.clientes.exception.ModeloNotFoundException;
import com.magic.clientes.model.Cliente;
import com.magic.clientes.repository.IClienteRepo;
import com.magic.clientes.service.IClienteService;
import com.magic.clientes.utils.StandardDeviation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.*;

@Service
public class ClienteService implements IClienteService {
    @Autowired
    IClienteRepo repo;

    @Override
    public Cliente createClient(Cliente cliente) {
        LocalDate ahora = LocalDate.now();
        Period periodo = Period.between(cliente.getFechaNac(), ahora);
        if(periodo.getYears() == cliente.getEdad()) {
            return repo.save(cliente);
        }else{
            throw new ModeloNotFoundException("TU EDAD NO COINCIDE CON LA FECHA DE NACIMIENTO" );
        }
    }

    @Override
    public List<Cliente> listClient() {
        return repo.findAll();
    }

    @Override
    public HashMap<String, Double> getKClient() {
        List<Cliente> cliente = listClient();
        HashMap<String, Double> hash = new HashMap<>();
        Double prom = cliente.stream().collect(averagingDouble(Cliente::getEdad));
        //Double prom1 = cliente.stream().mapToDouble(p-> p.getEdad().doubleValue()).summaryStatistics().getAverage();
        List<Integer> edades = cliente.stream().map(p -> p.getEdad()).collect(toList());
        Double k = StandardDeviation.computeStandardDeviation(edades);
        hash.put("Promedio", prom);
        hash.put("Desviacion Estandar", k);
        return hash;
    }

    @Override
    public Cliente getClient(Integer id) {
        if(repo.findById(id).isPresent()){
            return repo.findById(id).get();
        }else{
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
        }
    }
}
