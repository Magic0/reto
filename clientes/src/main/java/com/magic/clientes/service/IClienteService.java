package com.magic.clientes.service;

import com.magic.clientes.model.Cliente;

import java.util.HashMap;
import java.util.List;

public interface IClienteService {

    Cliente createClient(Cliente cliente);

    List<Cliente> listClient();

    HashMap<String, Double> getKClient();

    Cliente getClient(Integer id) throws Exception;
}
