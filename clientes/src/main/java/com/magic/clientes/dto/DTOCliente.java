package com.magic.clientes.dto;

import javax.validation.constraints.*;
import java.time.LocalDate;

public class DTOCliente {

    private Integer id;

    @Size(max =  200)
    private String nombre;

    @Size(max =  200)
    private String apellido;

    @Min(0)
    @Max(200)
    private Integer edad;

    @Past
    private LocalDate fechaNac;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public LocalDate getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(LocalDate fechaNac) {
        this.fechaNac = fechaNac;
    }
}
