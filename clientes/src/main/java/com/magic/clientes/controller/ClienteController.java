package com.magic.clientes.controller;

import com.magic.clientes.dto.DTOCliente;
import com.magic.clientes.model.Cliente;
import com.magic.clientes.service.IClienteService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/clientes")
public class ClienteController {
    @Autowired
    IClienteService service;

    @Autowired
    private ModelMapper mapper;

    @PostMapping(value = "/creacliente", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createClient(@Valid @RequestBody DTOCliente dtoRequest) throws Exception{
        Cliente client = mapper.map(dtoRequest, Cliente.class);
        Cliente c = service.createClient(client);
        DTOCliente dtoResponse = mapper.map(c, DTOCliente.class);
        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/clientes/obtenercliente/{id}").buildAndExpand(dtoResponse.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping("/kpideclientes")
    public ResponseEntity<HashMap<String, Double>> getKClient(){
        HashMap<String, Double> hash = service.getKClient();
        return new ResponseEntity<>(hash, HttpStatus.OK);
    }

    @GetMapping("/listarclientes")
    public ResponseEntity<List<DTOCliente>> listClient(){
        List<DTOCliente> lista =service.listClient().stream().map(p-> mapper.map(p,DTOCliente.class)).collect(Collectors.toList());
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @GetMapping("/obtenercliente/{id}")
    public ResponseEntity<DTOCliente> getClient(@PathVariable("id")Integer id) throws Exception{
        Cliente cliente = service.getClient(id);
        DTOCliente dtoResponse = mapper.map(cliente, DTOCliente.class);
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }
}
