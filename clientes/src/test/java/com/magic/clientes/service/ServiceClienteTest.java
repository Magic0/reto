package com.magic.clientes.service;

import com.magic.clientes.model.Cliente;
import com.magic.clientes.repository.IClienteRepo;
import com.magic.clientes.service.impl.ClienteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.*;
import java.time.LocalDate;
import java.util.Optional;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ServiceClienteTest {
    @Mock
    IClienteRepo repo;

    @InjectMocks
    ClienteService service = new ClienteService();

    @Test
    public void creaClienteTest(){
        Cliente cliente = new Cliente();
        cliente.setEdad(20);
        cliente.setId(2);
        cliente.setApellido("world");
        cliente.setNombre("hello");
        cliente.setFechaNac(LocalDate.parse("2002-01-08"));
        when(repo.save(any(Cliente.class))).thenReturn(cliente);
        Cliente creado = service.createClient(cliente);

        assertEquals(Optional.ofNullable(creado.getEdad()), Optional.ofNullable(cliente.getEdad()));
        assertEquals(Optional.ofNullable(creado.getId()), Optional.ofNullable(cliente.getId()));
        assertEquals(Optional.ofNullable(creado.getFechaNac()), Optional.ofNullable(cliente.getFechaNac()));
        assertEquals(Optional.ofNullable(creado.getNombre()), Optional.ofNullable(cliente.getNombre()));
        assertEquals(Optional.ofNullable(creado.getApellido()), Optional.ofNullable(cliente.getApellido()));

    }


}
