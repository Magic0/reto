package com.magic.clientes.controller;

import com.magic.clientes.dto.DTOCliente;
import com.magic.clientes.model.Cliente;
import com.magic.clientes.service.IClienteService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ClienteControllerTest {
    @InjectMocks
    private ClienteController controller = new ClienteController();

    @Mock
    private IClienteService service;

    @Mock
    private ModelMapper mapper;


    @Test
    public void listarClientesNotNullTest() {
        List<Cliente> lista = new ArrayList<>();
        when(service.listClient()).thenReturn(lista);
        ResponseEntity<List<DTOCliente>> response = controller.listClient();
        assertNotNull(response);
    }

    @Test
    public void getKClientesNotNullTest() {
        HashMap<String,Double> map = new HashMap<>();
        when(service.getKClient()).thenReturn(map);
        ResponseEntity<HashMap<String, Double>> response = controller.getKClient();
        assertNotNull(response);
    }
}
